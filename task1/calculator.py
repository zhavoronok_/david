def add(a, b):
	c = a + b
	mystr = str(a) + ' + ' + str(b) + ' = ' + str(c)
	return [c, mystr]

def subtract(a, b):
	c = a - b
	mystr = str(a) + ' - ' + str(b) + ' = ' + str(c)
	return [c, mystr]

def multiply(a, b):
	c = a * b
	mystr = str(a) + ' * ' + str(b) + ' = ' + str(c)
	return [c, mystr]

def divide(a, b):
	c = a / b
	mystr = str(a) + ' / ' + str(b) + ' = ' + str(c)
	return [c, mystr]