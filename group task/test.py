from datetime import datetime, timedelta
import templates
from flask import Flask
from flask import render_template
from flask import Flask, redirect, url_for, render_template, request

app = Flask(__name__)
file = 'out10.csv'
dataFormat = '%Y-%m-%d %H:%M:%S'
one_day = timedelta(1)


def read(file):
    f = open(file)
    
    data = f.read()

    f.close()

    return data


def parseString(str):
    return str.split(",")

def printArray(arr):
    for i in arr:
        print(i)

def retId(data):
    res = []
    for i in range(0, len(data)-1):
        if data[i][0] not in res:
            res.append(data[i][0])
    res.sort()
    return res

def resultBool(date, data, id):
    a = 'True'

    dateStart = date
    dateEnd = date + one_day
    for i in range(0, len(data)-1):
        if id == data[i][0]:
            if (
                dateStart < data[i][1] < dateEnd
                or data[i][1] < dateStart < data[i][2]
                or dateStart == data[i][1] and dateEnd == data[i][2]
                or dateStart == data[i][1] and dateEnd != data[i][2]
                or dateStart != data[i][1] and dateEnd == data[i][2]
            ):
                a = 'False'
                break
    return a

def check(hotel, dateStart, dateEnd):
  return (
    (hotel[1] <= dateStart < hotel[2]) 
    or  
    (hotel[1] < dateEnd <= hotel[2]) 
    or
    (dateStart <= hotel[1] < dateEnd) 
    or
    (dateStart < hotel[2] <= dateEnd)
  )

data = read(file)
data = data.split('\n')

for i in range(1, len(data)-1):

        data[i] = data[i].split(',')
        data[i][1] = data[i][1].replace('T', ' ')[:data[i][1].rfind('.')]
        data[i][1] = datetime.strptime(data[i][1], dataFormat)

        data[i][2] = data[i][2].replace('T', ' ')[:data[i][2].rfind('.')]
        data[i][2] = datetime.strptime(data[i][2], dataFormat)

data = data[1:]

res = retId(data)

table = []

table.append([])
table[0].append('ResourseId')

start = '2021-01-01'
start = datetime.strptime(start, '%Y-%m-%d')

for i in range(1, 366):
    table[0].append(start)
    start += one_day

for i in range(1, len(res)+1):
    table.append([])
    table[i].append(res[i-1])
    for j in range(1,366):
        # table[i][0] = id
        table[i].append(resultBool(table[0][j], data, table[i][0]))


@app.route('/')
def test():
    return render_template('table.html', data=table)

if __name__ == "__main__":
    app.run(debug = True)
