def findPosition(array, key):
    if len(array) == 0: 
    	return 0
    for i in range(len(array)):
        if array[i] >= key:
            return i
    return len(array)
  