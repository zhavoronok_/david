from program3 import findPosition
import test

test.test(findPosition([1,3,5,6], 5), 2)
test.test(findPosition([1,3,5,6], 2), 1)
test.test(findPosition([1,3,5,6], 7), 4)
test.test(findPosition([1,3,5,6], 0), 0)