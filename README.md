### Жаворнок Давид, 2 курс, 8 группа

### Индивидуальные задания:
1. [Задание по алгоритмизации 1](https://bitbucket.org/zhavoronok_/david/src/master/task1/)
2. [Задание по алгоритмизации 2](https://bitbucket.org/zhavoronok_/david/src/master/task2/)
3. [Задание по алгоритмизации 3](https://bitbucket.org/zhavoronok_/david/src/master/task3/)

### Групповое задание:
Код группового задания можно посмотреть [здесь](https://bitbucket.org/zhavoronok_/david/src/master/group%20task/).
Результат работы группового задания можно посмотреть, перейдя по [ссылке](https://vich-2020-mmf-2-course.herokuapp.com/)
