from program2 import countingBits 
import test

test.test(countingBits(2), [0,1,1])
test.test(countingBits(5), [0,1,1,2,1,2])
test.test(countingBits(5), [0,1,1,2,1])